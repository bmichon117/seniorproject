import javax.imageio.ImageIO;
import java.awt.*;
import java.awt.event.KeyEvent;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;

public class Player extends Entity{

    public Player(Background game) {
        this.game = game;
        this.bounds = new Rectangle(x,y,width,height);
    }


    private int x = 30;
    private int y = 273;
    private int moveX = 0, moveY = 0;
    private int height = 27;// height and width can be used for sprite sheet and in game position
    private int width = 30;


    private Rectangle bounds; //will be used for collision detection
    private boolean isPlayer = true;
    private boolean isEnemy = false;
    private int hitPoints = 100;

    private BufferedImage playerSprite = obtainImage();
    private Background game;

    @Override
    public Rectangle getBounds(){
        return this.bounds;
    }

    public BufferedImage getPlayerSprite(){
        return playerSprite;
    }

    @Override
    public boolean isPlayer(){
        return isPlayer;
    }

    @Override
    public boolean isEnemy(){
        return isEnemy;
    }

    private void setHitPoints(int HP){
        this.hitPoints = HP;
    }

    private int getHitPoints(){
        return this.hitPoints;
    }

    @Override
    public void doDamage(int damage){
        setHitPoints(getHitPoints()-damage);
    }

    public void healHP(int health){
        if(getHitPoints()+ health > 100){
            setHitPoints(100);
        }else{
            setHitPoints(getHitPoints()+health);
        }
    }


    private BufferedImage obtainImage(){
        try {
            playerSprite = new BufferedImage(width,height,BufferedImage.TYPE_INT_ARGB);
            playerSprite = ImageIO.read(new File("C:\\users\\bryan\\Pictures\\alien sprite(1).png"));
        } catch (IOException ioe) {
            ioe.printStackTrace();
        }
        return playerSprite;
    }

    public void move() {
        if (x + moveX >= 0 && x + moveX<= 170)
            x = x + moveX;
    }

    public void keyReleased(KeyEvent e) {
        moveX = 0;
        moveY = 0;
    }

    public void keyPressed(KeyEvent e) {
        try {
            if (e.getKeyCode() == KeyEvent.VK_LEFT)
                moveX = -1;

            if (e.getKeyCode() == KeyEvent.VK_RIGHT)
                moveX = 1;

            if (e.getKeyCode() == KeyEvent.VK_SPACE) {
                game.spawnPlayerBullet();
                System.out.println("bullet should spawn");
            }

            if (e.getKeyCode() == KeyEvent.VK_ESCAPE) {
                System.out.println("Exiting game...");
                System.exit(0);
            }
        }catch(Exception ex){
            ex.printStackTrace();
        }
    }
    @Override
    public int getX(){
        return this.x;
    }

    @Override
    public int getY(){
        return this.y;
    }

    public void draw(Graphics g) {
        g.drawImage(getPlayerSprite(),x,y,null);

        /*
        use the follow code in case testing on another computer and don't have image file
        then comment out the above code
        g.setColor(Color.GREEN);
        g.fillRect(x,y,width,height);
        */

    }


}

