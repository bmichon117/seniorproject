import java.awt.Canvas;
import java.awt.Graphics;
import java.awt.Color;
import java.awt.Rectangle;

public class PlayerBullet extends Canvas {

    private int x = -10;
    private int y = -10;
    private int width = 5;
    private int height = 10;
    private boolean spawned = false;
    Background game;
    private Rectangle bounds = getBounds();
    private boolean usedUp = false;

    public PlayerBullet(Background game){
        this.game = game;
        this.bounds = new Rectangle(this.x, this.y, width, height);
    }
    public void use(){
        usedUp = true;
    }

    @Override
    public Rectangle getBounds(){
        return this.bounds;
    }

    public void setX(int x){
        this.x = x;
    }
    public void setY(int y){
        this.y = y;
    }

    public int getX(){
        return this.x;
    }

    public int getY(){
        return this.y;
    }

    public void move(){
        if(usedUp == true){
            y = -10;
            x = -10;
        }else{
            if (this.y > 0) {
                this.y -= 2;
                System.out.println("Bullet Moving");
            } else {
                this.spawned = false;
            }
        }

    }
    public boolean isSpawned(){
        return spawned;
    }

    public void spawnBullet(int x, int y){
        this.x = x;
        this.y = y;
        spawned = true;
        usedUp = false;
    }

    public void draw(Graphics g){
        g.setColor(Color.YELLOW);
        g.fillRect(x,y,width, height);
    }
}
