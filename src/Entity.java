import java.awt.Canvas;
import java.awt.Rectangle;

// this class will handle the bulk of what the characters do, each extension for each type of character
// will have their specifics like sprite sheet grabbing and specific updates which are what their
// game-play behavior includes
public class Entity extends Canvas{

    public Entity(){}

    private boolean isDead = false;
    //will keep track if object is dead so that after updating objects, dead objects can be removed
    //if removing then updating, updates will not be kept track of correctly

    private int x;
    private int y;

    private int height;
    private int width;
    private Rectangle bounds = getBounds();

    private boolean isPlayer = false;
    private boolean isEnemy = true;

    private int damageAmount;
    private int hitPoints;


    //I'm thinking I need to have a method that knows what dimensions to look for and grabs the sprites
    // that way; have an abstract getSprites() method and then each extension uses pre-determined dimensions

    public Rectangle getBounds(){
        Rectangle bounds = new Rectangle(this.x, this.y, this.width, this.height);
        return bounds;
    }

    public boolean isDead(){
        return isDead;
    }

    public void killEntity(){ isDead = true; }

    //if for some extraneous situation an entity needs to be revived, but it still needs to be added to the
    //Entity arraylist and therefore should never be used unless in extreme circumstances
    public void resurrectEntity(){ isDead = false; }

    public int getX(){
        return this.x;
    }

    public int getY(){
        return this.y;
    }

    protected void setX(int setX){ x = setX; }

    protected void setY(int setY){ y = setY; }

    public boolean isPlayer(){
        return isPlayer;
    }

    public boolean isEnemy(){
        return isEnemy;
    }

    public int getDamage(){
        return damageAmount;
    }

    public void doDamage(int damage){
        hitPoints -= damage;
    }

}
