import javax.imageio.ImageIO;
import java.awt.*;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;

public class ShipInvader extends Entity {

    public ShipInvader(Background game){
        this.game = game;
        this.bounds = new Rectangle(x,y,DIAMETER,DIAMETER);
    }

    private static final int DIAMETER = 30;
    private int x = 0;
    private int y = 100;
    private int moveX = 1, moveY = 1;
    private int height = DIAMETER;
    private int width = DIAMETER;
    private boolean movingRight = false;
    private int damageAmount = 10;
    private Background game;
    private boolean dead = false;
    private Rectangle bounds = getBounds();
    private BufferedImage shipImage = obtainImage();

    @Override
    public int getX(){
        return this.x;
    }

    @Override
    public int getY(){
        return this.y;
    }


    private BufferedImage obtainImage(){
        try {
            shipImage = new BufferedImage(DIAMETER,DIAMETER,BufferedImage.TYPE_INT_ARGB);
            shipImage = ImageIO.read(new File("C:\\users\\bryan\\Pictures\\rocket ship invader.png"));
        } catch (IOException ioe) {
            ioe.printStackTrace();
        }
        return shipImage;
    }
    private BufferedImage obtainDestroyedImage(){
        try {
            shipImage = new BufferedImage(DIAMETER,DIAMETER,BufferedImage.TYPE_INT_ARGB);
            shipImage = ImageIO.read(new File("C:\\users\\bryan\\Pictures\\" +
                    "rocket ship invader destroyed.png"));
        } catch (IOException ioe) {
            ioe.printStackTrace();
        }
        return shipImage;
    }
    public BufferedImage getShipImage(){
        return shipImage;
    }

    public void kill(){
        this.dead = true;
    }

    @Override
    public Rectangle getBounds(){
        return this.bounds;
    }

    @Override
    public int getDamage(){
        return damageAmount;
    }

    public void move() {
        if(dead == false) {
            if (x >= 170) {
                movingRight = false;
            } else if (x <= 0)
                movingRight = true;

            if (movingRight) {
                x = x + moveX;
            } else {
                x = x - moveX;
            }
        }else{
            if(y < 273){
                y = y + 5;
            }
            if(y >= 273){
                shipImage = obtainDestroyedImage();
            }
        }
    }

    public void draw(Graphics g){
            g.drawImage(getShipImage(),x,y,null);
            /*
            use the follow code in case testing on another computer and don't have image file
            then comment out the above code
            g.setColor(Color.RED);
            g.fillRect(x,y,DIAMETER,DIAMETER);
            */
    }
}
