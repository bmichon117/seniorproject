import java.awt.Rectangle;

public class PhysObject
{

    public PhysObject(){}
    //

    private int x;
    private int y;
    private int height;
    private int width;
    private Rectangle bounds = getBounds();

    private boolean isObject = true;

    public boolean isObject(){
        return isObject;
    }

    public Rectangle getBounds(){
        Rectangle bounds = new Rectangle(this.x, this.y, this.width, this.height);
        return bounds;
    }


}
