import javax.sound.sampled.UnsupportedAudioFileException;
import javax.sound.sampled.LineUnavailableException;
import java.io.IOException;

public class Main {

    public static void main(String[] args) {
        try{
            try{
                try {
                    Background game = new Background();
                    run(game);
                }catch(LineUnavailableException lue){
                    lue.printStackTrace();
                }
            }catch(IOException ioe){
                ioe.printStackTrace();
            }
        }catch(UnsupportedAudioFileException uafe){
            uafe.printStackTrace();
        }
    }

    public static void run(Background game){
        final int numTicks = 60;
        long startTime = System.nanoTime(), currTime = 0, lastTime = startTime, elapsedTime = 0;
        int tickCount = 0, frames = 0, updates = 0;
        while(true){
            currTime = System.nanoTime();
            elapsedTime = currTime - lastTime;
            lastTime = currTime;
            tickCount = (int)(((double) elapsedTime / 1000) * numTicks);

            while (tickCount > 0) {
                game.update();
                updates++;
                tickCount--;
            }
            frames++;

            if(currTime - startTime >= 1000000000){
                System.out.println("Frames: " + frames + " Updates: " + updates);
                startTime = System.nanoTime();
                frames = 0;
                updates = 0;
            }
        }
    }
}
